# HLA-3Diff
***

## Présentation du projet

Ce projet vise à établir un score de compatibilité entre donneur et receveur dans le cadre de greffes.
Ce score est estimé à partir des différences structurales entre les allèles HLA (de classe I uniquement pour le moment) entre un donneur et un receveur.
Pour ce faire, il a été nécessaire de modéliser l'ensemble des allèles HLA (de classe I), puis de les superposer pour obtenir un score de différence structurale, le rmsd. Le score est déduit à partir des rmsd obtenus.
Enfin, un prototype php est proposé pour le calcul de score.

![Schéma du projet](/results/presentation/Schema_projet.png)

Pour plus de détails, veuillez vous référer à mon mémoire de stage, présent dans ce git (`./communication/Memoire&Presentation/Mémoire_MOREAU_Augustin.pdf`).
Enfin, veuillez noter que 2 versions du script de modélisation cohabitent, `modelise.py` et `test_MSA.py`. Le premier, qui a servi à produire les résultats actuels de ce projet, est beaucoup plus lent ; tandis que le second réalise un unique alignement multiple, bien plus rapide, mais est théoriquement un tout petit peu moins fiable (des erreurs peuvent survenir dans l'alignement multiple).


## Arborescence du projet
L'architecture du projet respecte globalement celle encouragée par le CR2TI. Le projet comprend les dossiers suivants :

### communications
Ce dossier contient quelques articles et documentations utiles pour s'approprier le projet, ainsi que le mémoire et la présentation associés au projet.

### raw-data
Ce dossier comprend les données brutes, à savoir les fichiers fasta et pdb des structures templates, issues la PDB, ainsi que les fichiers fasta des allèles HLA provenant de IMGT/HLA.
Note : Certains fichiers pdb ne sont pas tout à fait des données brutes. En effet, les fichiers pdb contenant plusieurs structures ont été séparés "à la main" en plusieurs fichiers avec une protéine HLA chacun. Ces fichiers sont cependant laissés ici pour des raisons de praticité.

### working_data
Ce dossier comprend la plupart des fichiers intermédiaires, notamment :
- Les fichiers "liste_hla" sont les listes des templates sélectionnés sur la PDB.
- Les fichiers "all_fasta" sont la liste des templates avec leurs séquences, annotés tel qu'identifiés dans la PDB puis après vérification.
- Les fichiers "ClustalW" sont le résultat d'alignement multiple des templates, destinés à réaliser une phylogénie grâce à clustalW et Jalview. Cette approche n'a pas été creusée, mais les fichiers sont restés car l'idée est intéressante.
- Les fichiers "alleles_geneX" sont les listes des allèles modélisés pour chaque gène, avec l'identifiant IMGT/HLA et la nomenclature en 2 champs.
- Le fichier "liste_alleles_epi" est la liste des allèles les plus fréquents, couvrant plus de 99% de la population.
- Le fichier "hla_prot_inline" est un fichier issu du git de IMGT/HLA et recense tous les allèles HLA (avec nomenclature à champs) avec leur séquence répertoriés au moment de son téléchargement (avril 2022).
- Les archives "hla_X.tar.gz" contiennent les structures d'allèles modélisées d'un gène HLA.
Note : Actuellement, seules les structures des HLA-A sont présentes dans le projets, selon l'ancienne méthode et la nouvelle.
Note : Les structures HLA-A modélisées selon l'ancienne méthode ont été refaite récemment dans le but d'évaluer le temps de calcul, il faudrait resuperposer ces structures (avec `rmsd_hla_I.py`) et comparer le tableau de rmsd obtenus avec celui présent dans ce projet pour vérifier que rien n'a été changé entre-temps.

### scripts
Ce dossier contient les scripts utilisés par le projet. La plupart d'entre eux contiennent une ligne de commentaire explicant leur rôle. Les plus importants sont :
- "modelise.py" qui génère le fichier d'alignement utilisé par modeller et lance ce dernier pour exécuter les modélisations.
- "rmsd_hla_X.py" qui superposent tous les couples d'allèles 2 à 2 et stockent les rmsd obtenus dans un tableau csv.
Ces programmes ainsi que "heatmap_rmsd.R" sont accompagnés d'un script sh nécessaires à une execution sur Bird (Voir [la documentation](https://bird2cluster.univ-nantes.fr/page/Jobs%20submission/)). Ce sont ceux qui nécessitent une grosse puissance de calcul ainsi que les outils particuliers nécessitant un certain environnement conda.
Note : Les scripts et fichiers temporaires relatifs à modeller sont tous dans un sous-dossier modeller, pour des raisons de praticité.
Note : Le prototype php et les data nécessaire à son fonctionnement est situé dans un sous-dossier, ici. Ce répertoire doit conserver son intégrité pour être exécuté dans un serveur.

### results
Ce dossier contient les résultats du projets :
- Le dossier "Template_validation" contient les résultats des analyses des structures sélectionnés dans la PDB. Ce dossier est ancien et il est possible que tous les tableaux de rmsd ne soient plus tout à fait à jour.
- Le dossier "rmsd_tab" contient les tableaux de rmsd pour chacun des gènes HLA de classe I, avec tous les allèles modélisés ou seulement les plus fréquents.
- Le dossier "R_heatmap" contient les heatmap issues des tableaux de rmsd du dossier "rmsd_tab".
- Le dossier "Validation" contient les résultats de la comparaison du score avec HLA-Epi (régression linéaire), et avec les données biologiques de la cohorte Divat (régression logistique et régression de cox). Seuls les résultats de ces analyses statistiques sont présent dans ce git, ayant été effectuées par d'autres membres de l'équipe.
- Le dossier "présentation" contient quelques figures et images supplémentaires, générées à des fins didactiques.
Note : Les tableaux de rmsd présents sont ceux obtenus à partir des anciennes versions du script de modélisation, ainsi que celui avec la nouvelle version ("v2") pour les HLA-A uniquement.


## Utilisation

### Installation
Quelques scripts de ce projet nécessitent certains packages installables avec Conda (ou Miniconda). Les packages utilisés sont :
- pymol (schrodinger)
- modeller (salilab)
- biopython (conda-forge) (? Je ne sais plus si cela est fait automatiquement ou non)
- clustalo (bioconda)
Le plus simple est de recréer l'environnement hla3diff à partir du fichier yml présent dans le dossier "working_data", grâce à la commande `conda env create -f ./working_data/environment.yml`

Certains scripts R nécessitent également des librairies :
- gplots
- ggplot2
- RColorBrewer
En cas d'utilisation du cluster BiRD, il est possible d'utiliser l'interface RStudio proposée. Attention à suivre [la documentation](https://bird2cluster.univ-nantes.fr/page/Rstudio%20on%20BiRD/).

### Exécution
Pour générer les tableaux de rmsd, deux scripts sont à exécuter.

Le premier est `modelise.py`, ou sa nouvelle version `test_MSA.py`, qui prennent un nom de gène ('A', 'B' ou 'C') en argument. Son exécution (par exemple pour le gène A) sur BiRD se fait à l'aide de la commande
```
qsub run_test_MSA.sh A
```
Cette commande prend ~8h (~14h avec l'ancienne version `modelise.py`).

Le deuxième script à exécuter est `rmsd_hla_I.py`, avec à nouveau le nom du gène comme argument. Son exécution se fait à l'aide de la commande
```
qsub run_qsub_rmsd.sh A
```
Cette commande prend ~8h.


## Authors and acknowledgment
Ce projet a été réalisé par Augustin MOREAU, sur une idée de Nicolas VINCE et Léo BOUSSAMET.
La comparaison avec HLA-Epi à l'aide d'une régression linéaire a été réalisée par Léo BOUSSAMET.
La confrontation aux résultats biologiques à l'aide d'une régression logistique et d'une régression de Cox a été réalisée par Justine BARON.
Je remercie l'ensemble de l'équipe 3 du CR2TI pour son accompagnement et son support.
