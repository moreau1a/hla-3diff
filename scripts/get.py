#!/usr/bin/env python3

import requests


# Script qui vérifie le r-Factor des structures utilisées

out = open("../results/analyse_rmsd.csv", 'w')
with open("../results/moy_rmsd.csv", 'r') as moy:
    for line in moy:
        id = line.split(';')[0].split('_')[0]
        js = requests.get("https://www.ebi.ac.uk/pdbe/api/pdb/entry/experiment/%s" %id).json()
        rF = (js[id][0]["r_factor"])
        out.write("%s;%s\n" %(line.strip(), rF))
out.close()