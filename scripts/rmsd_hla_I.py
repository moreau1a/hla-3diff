#!/mnt/c/Users/morea/AppData/Local/Schrodinger/PyMOL2/Python.exe

import sys
import os
import multiprocessing as mp
from functools import partial
from pymol import cmd as pml


# Ce script compare des structures modélisées d'allèles hla A entre elles et fait un tableau de rmsd.

# Entrée : Le nom d'un gène HLA, les structures modélisées précédemment
# Sortie : Le tableau de rmsd correspondant (dans results/rmsd_tab)

def superp(deja, x, y):
    if x == y:
        return ",0" # diagonale, structure contre elle même, rmsd = 0
    elif y in deja:
        return "," # triangle bas, symétrique
    else:
        obj1 = x
        pml.load("../working_data/pdb_files/structs_%s/%s.pdb" %(geneHLA, obj1))
        # Sélection de la partie alpha 1&2 pour la superposition
        pml.create("%s_A" %obj1, "%s and chain A and residue 3-13+20-38+45-85+92-103+109-127+132-178" %obj1)

        obj2 = y
        pml.load("../working_data/pdb_files/structs_%s/%s.pdb" %(geneHLA, obj2))
        # Sélection de la partie alpha 1&2 pour la superposition
        pml.create("%s_A" %obj2, "%s and chain A and residue 3-13+20-38+45-85+92-103+109-127+132-178" %obj2)

        try:
            rmsdFull = pml.super(obj1, "%s_A" %obj2)
            rms = round(rmsdFull[0], 3)
        except:
            print("Erreur de superposition entre les structures %s et %s" %(obj1, obj2))
            exit(1)

        pml.delete(obj1)
        pml.delete("%s_A" %obj1)
        pml.delete(obj2)
        pml.delete("%s_A" %obj2)
        return ",%s" %rms # triangle haut

if __name__ == "__main__":
    global geneHLA; geneHLA = sys.argv[1]
    # Création d'une liste des structures à comparer
    listeStruct = []
    for fic in os.listdir("../working_data/pdb_files/structs_%s/" %geneHLA):
        # Si on veut réaliser le tableau avec uniquement les allèles les plus fréquents, tels qu'identifiés par HLA-Epi
        # if os.popen("grep %s ../working_data/alleles_gene%s.txt" %(fic.split('.')[0], geneHLA)).read().split(' ')[1].strip() in open('../working_data/liste_alleles_epi.txt').read():
            listeStruct.append(fic.split('.')[0])

    # Écriture d'un tableau de différences rmsd entre chaque structures
    deja = [] # liste des structures déjà traitées pour éviter de recalculer le rmsd en mirroir dans le triangle bas du csv.
    with open("../results/rmsd_tab/rmsd_hla_%s.csv" %geneHLA, 'w') as r:
        for x in listeStruct:
            r.write(",%s" %x) # première ligne du tableau
        for x in listeStruct: # les autres lignes
            r.write("\n%s" %x) # première colone du tableau
            print("Début de %s" %x)
            with mp.Pool(mp.cpu_count()) as pool:
                res = pool.map(partial(superp, deja, x), listeStruct)
            for i in range(0, len(listeStruct)):
                r.write(res[i])
            deja.append(x)
