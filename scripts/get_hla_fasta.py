#!/usr/bin/env python3

import os


# Ce script extrait le ficher fasta de IMGT/HLA et l'écrit avec les séquences en une ligne

os.system("tar xf ../raw_data/IMGTHLA-Latest.tar.gz -C ../raw_data/ --strip-components=2 IMGTHLA-Latest/fasta/hla_prot.fasta")
seq = ""
with open("../raw_data/hla_prot.fasta", 'r') as fasta:
    with open("../working_data/hla_prot_inline.fasta", 'w') as js:
        for line in fasta:
            if '>' in line:
                if seq != "":
                    js.write(seq+"\n")
                    seq = ""
                js.write(line)
            else:
                seq+=line.strip()
os.remove("../raw_data/hla_prot.fasta")