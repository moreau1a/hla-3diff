# Script options
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -M augustin.moreau1@etu.univ-nantes.fr
#$ -m ea

conda activate hla3diff

if [ $# != 1 ]; then
    echo "Veuillez entrer un gene comme argument pour ce programme" >&2
    exit 1
fi

GENE=$1
time python3 test_MSA.py ${GENE}
