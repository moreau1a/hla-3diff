#!/usr/bin/env python3

import os
import sys
import json
import shutil
from subprocess import DEVNULL, STDOUT, check_call
import multiprocessing as mp
from functools import partial
from Bio import SeqIO # fasta reader
from Bio.PDB import MMCIFParser # cif reader
from Bio.pairwise2 import align, format_alignment
from Bio.Data.IUPACData import protein_letters_3to1 as prot3to1


# Ce script lance la modélisation de tous les allèles d'un gène donné en paramètre.
# Il réalise d'abord un alignement multiple des différents templates à utiliser, cherche les allèles à modéliser puis,
# pour chacun d'eux et en parallèle, génère un fichier pir en le réalignant avec les templates et lance la modélisation.
# Cette méthode est remplacée par un alignement multiple de toutes les séquences dans test_MSA.py.

# Entrée : Un nom de gène HLA, la liste des templates (en dur dans le code), les fichiers de structures de ces templates.
# Sortie : Les structures modélisés des allèles HLA, dans une archive dans working_data.

def modeliser(listeModelsJs, id): # modélisation des structures avec un template composé
    # Copie des fichiers pour travail en parallèle
    shutil.copy("maf_templates.fasta", "maf_%s.fasta" %id)
    # Ajout de la séquence du hla à modéliser à celles des templates puis réalignement multiple
    with open("maf_%s.fasta" %id, 'a') as fic_maf:
        fic_maf.write(">querry\n%s" %dicSeqHLA[id])
    os.system("clustalo --dealign -i ./maf_%s.fasta -o ./maf_%s.fasta --force" %(id, id))
    # Récupération de la séquence et réécriture du fichier pir correspondant
    with open("./maf_%s.fasta" %id, 'r') as fic_maf:
        aln_dict = SeqIO.to_dict(SeqIO.parse(fic_maf, "fasta"))
    with open("%s.pir" %id, 'w') as fic_pir:
        with open("Fichier.pir", 'r') as fic_pir_template:
            for line in fic_pir_template:
                if ">" in line:
                    idModele = line.strip().split(';')[1]
                    fic_pir.write(line)
                elif "structure" in line:
                    fic_pir.write(line)
                else:
                    seq = line.split('/')
                    seq.pop(0)
                    fic_pir.write("%s/%s" %(aln_dict[idModele].seq, '/'.join(seq)))
        fic_pir.write(">P1;%s\nsequence:%s:1:A:%s:A::::\n" %(id, id, len(dicSeqHLA[id])))
        fic_pir.write("%s*\n" %aln_dict["querry"].seq)
    check_call(["python3", "./launch_modeller.py", "%s.pir" %id, listeModelsJs, id], stdout=DEVNULL, stderr=STDOUT)
    # Nettoyage des fichiers temporaires
    for ext in ["D00000001", "V99990001", "ini", "rsr", "sch"]:
        os.remove("%s.%s" %(id, ext))
    os.remove("%s.pir" %id)
    os.remove("maf_%s.fasta" %id)

if __name__ == "__main__":
    geneHLA = sys.argv[1]
    if geneHLA == 'a': # jeu test : un template retenu pour chaque allàle avec au moins une structure
        listeModeles = ["3bo8", "6trn", "3oxr", "3oxs", "6joz", "7jyv", "6j1w", "6j2a", "6ei2"]
    elif geneHLA == "A":
        # listeModeles = ["3bo8", "4u6y", "4u6x", "6trn", "1i4f", "6o53", "5ddh", "6o4z", "2gtw", "6o51", "6o4y", "7mj7", "3utq", "2gtz", "7mj9", "7mj8", "1duz", "5c0g",
        # "5n1y", "5c0f", "5c0e", "5c0i", "5eu5", "5nmh", "2v2w", "2v2x", "2vll", "3bgm", "3pwn", "3v5h", "5c0j", "3bh8", "3fqn", "3kla", "3pwl", "4wj5", "5nmk", "7p3d",
        # "3qfd", "5c0d", "2git", "3bh9", "3fqr", "3fqx", "3o3d", "3pwj", "5e00", "3d25", "4no3", "5n6b", "2gt9", "4i4w", "5meo", "6opd", "1i7u", "1tvb", "3fqt", "3fqu",
        # "3o3a", "7kgq", "7kgp", "7kgr", "7kgs", "3mrb", "3mrk", "3gso", "3mrr", "3mrd", "3mre", "3mrg", "3ft2", "3gsu", "3mrc", "6tdp", "6tdq", "6tdo", "6tds", "6tdr",
        # "6q3k", "3oxr", "3oxs", "6joz", "1x7q", "7m8t", "2hn7", "6jp3", "5grd", "7jyv", "7mja", "4f7t", "7ejm", "3wl9", "6j1w", "6j2a", "6j29", "6ei2"]
            # ! Plusieurs structures dans certains fichiers pdb
        listeModeles = ["3bo8", "4u6y", "4u6x", "6trn", "1i4f", "6o53", "5ddh", "6o4z", "2gtw_1", "2gtw_2", "6o51", "6o4y", "7mj7", "3utq", "2gtz_1", "2gtz_2", "7mj9",
        "7mj8", "1duz_1", "1duz_2", "5c0g", "5n1y", "5c0f", "5c0e", "5c0i", "5eu5", "5nmh", "2v2w_1", "2v2w_2", "2v2x_1", "2v2x_2", "2vll_1", "2vll_2", "3bgm", "3pwn_1",
        "3pwn_2", "3v5h_1", "3v5h_2", "5c0j", "3bh8", "3fqn", "3kla_1", "3kla_2", "3pwl_1", "3pwl_2", "4wj5_1", "4wj5_2", "5nmk", "7p3d", "3qfd_1", "3qfd_2", "5c0d",
        "2git_1", "2git_2", "3bh9", "3fqr", "3fqx", "3o3d_1", "3o3d_2", "3pwj_1", "3pwj_2", "5e00", "3d25", "4no3", "5n6b_1", "5n6b_2", "2gt9_1", "2gt9_2", "4i4w",
        "5meo", "6opd", "1i7u_1", "1i7u_2", "1tvb_1", "1tvb_2", "3fqt", "3fqu", "3o3a_1", "3o3a_2", "7kgq", "7kgp", "7kgr", "7kgs", "3mrb", "3mrk", "3gso", "3mrr",
        "3mrd", "3mre", "3mrg", "3ft2", "3gsu", "3mrc", "6tdp", "6tdq_1", "6tdq_2", "6tdo", "6tds_1", "6tds_2", "6tdr_1", "6tdr_2", "6q3k", "3oxr", "3oxs", "6joz",
        "1x7q", "7m8t", "2hn7", "6jp3", "5grd", "7jyv", "7mja", "4f7t_1", "4f7t_2", "7ejm", "3wl9", "6j1w", "6j2a", "6j29", "6ei2"]
    elif geneHLA == 'B':
        listeModeles = ["6at5", "5eo0", "4u1h", "5wmp", "5wmo", "3vcl", "4qrs", "4qrt", "4qru", "4qrq", "3spv", "5wmr", "3skm", "5wmq", "3sko", "6p2c", "6p2f", "6p27",
        "6p23", "6p2s", "3x13", "5txs", "1xr9", "6vb2", "6vb1", "6uzm", "6mt3", "4xxc", "4jqv", "6pyl", "6pz5", "6pyv", "5def", "6y2a", "6y26", "2a83", "5ib2", "6pyj",
        "1ogt", "1uxs", "4g8i", "4g9d", "6y28", "6vqe", "3b6s", "6pyw", "1k5n", "3czf", "6y29", "6y2b", "6y27", "1uxw", "3bp7", "3d18", "7m8u", "1xh3", "1zhk", "4prn",
        "1zsd", "2cik", "3lko", "3lkp", "3lkq", "4pr5", "2fyy", "5xos", "3bwa", "1zhl", "3vfv", "3vfu", "4pre", "2nw3", "3bw9", "4prb", "4prd", "2axf", "3vfn", "3vfo",
        "6mt6", "6mt4", "6mt5", "5iek", "5ieh", "3ln4", "4u1m", "4u1j", "4u1n", "1m6o", "3kpm", "3l3i", "3dx6", "3l3d", "3dx7", "1n2r", "6mtl", "1syv", "4lcy_1", "4lcy_2",
        "6v2o", "6bxp", "3vri", "5t6x", "5t6y", "3vh8_1", "3vh8_2", "5vue", "3x12", "6v2p", "2bvp", "6v2q", "2bvo", "5vwd", "5vwf", "2hjl", "5vwh", "4u1s"]
    elif geneHLA == 'C':
        listeModeles = ["6jto", "5w6a_1", "5w6a_2"]

    ### Création du fichier pir : séquences des templates
    # Récupération des séquences des templates
    global dicTemplates; dicTemplates = {} # stocke les infos de la protéine
    for idModele in listeModeles:
        parser = MMCIFParser(QUIET = True)
        data = parser.get_structure(idModele, "../../raw_data/pdb_files/%s.cif" %idModele)
        model = data.get_models()
        models = list(model) # liste des modèles
        chains = list(models[0].get_chains()) # liste des chaines du premier modèle
        dicTemplates[idModele] = ["", {}]
            # premier élément de la liste = numéro du premier acide aminé dans le fichier cif, utile pour modeller
            # deuxième élément = dico des chaines et séquence de la protéine
        for i in range(0, len(chains)): # numérotation des chaines /!\ la ch A de la PDB pour 6bxq ne correspond pas à la chaine alpha du hla /!\
            residue = list(chains[i].get_residues())
            dicTemplates[idModele][1][i] = ""
            for AA in residue: # un AA se présente sous la forme '<Residue GLY het=  resseq=1 icode= >'
                if dicTemplates[idModele][0] == "": # Ne doit être réalisé qu'une fois, pour connaitre le numéro du 1er acide aminé
                    dicTemplates[idModele][0] = str(AA).split()[3].split('=')[1]
                if str(AA).split()[2] == "het=": # si la valeur est différente de ' ', l'acide aminé ne figure pas dans la structure et gêne modeller
                    dicTemplates[idModele][1][i] += prot3to1[str(AA).split()[1].capitalize()] # enregistrement de l'acide aminé
    
    ## Etape d'alignement multiple des séquences
    # Ecriture du fichier fasta de toutes les séquences de chaine A
    with open("templates.fasta", 'w') as fasta:
        for idModele in dicTemplates:
            fasta.write(">%s\n%s\n" %(idModele, dicTemplates[idModele][1][0]))
    # Execution de l'alignement multiple avec clustalo
    os.system("clustalo -i templates.fasta -o maf_templates.fasta --force")
    # Récupération des séquences alignés dans un dictionnaire
    with open("maf_templates.fasta", 'r') as aln_file:
        aln_dict = SeqIO.to_dict(SeqIO.parse(aln_file, "fasta"))
    # Supression des fichiers fasta intermédiaires
    os.remove("templates.fasta")
    # os.remove("maf_templates.fasta")

    # Ecriture du fichier pir
    with open("./Fichier.pir", 'w') as fic_pir:
        for idModele in aln_dict:
            dicTemplates[idModele][1][0] = str(aln_dict[idModele].seq) # récupération de la séquence alignée
            seqModele = "/".join(dicTemplates[idModele][1].values())
            chFin = '' # Dernière chaine, mais pas obligatoire visiblement
            fin = str(len(seqModele)-len(dicTemplates.values())) # Numéro du dernier acide aminé, pas sûr que ce soit vraiment nécessaire non plus
            fic_pir.write(">P1;%s\nstructureX:%s:%s:A:%s:%s::::\n%s*\n" %(idModele, idModele, dicTemplates[idModele][0], fin, chFin, seqModele))


    ### Création du fichier pir : protéine hla à modéliser
    # Chargement des fastas de IMGT/HLA
    with open("../../working_data/hla_prot_inline.fasta", 'r') as fastaFile:
        fasta_dict = SeqIO.to_dict(SeqIO.parse(fastaFile, "fasta"))

    # Sélection des ids des protéines à modéliser
    ids = [] # Liste des ids hla des allèles à modéliser
    deja = [] # Liste des allèles en 2 digits des allèles déjà retenus
        # (pour pas avoir plus de 2 digit comme A*02:01:01 et A*02:01:02, aucun intérêt du point de vue protéique, on modéliserait deux fois la même chose)
    with open("../../working_data/alleles_gene%s.txt" %geneHLA, 'w') as recap:
        for id_hla in fasta_dict:
            if fasta_dict[id_hla].description.split()[1][0] == geneHLA:
                allele = "%s:%s" %(fasta_dict[id_hla].description.split()[1].split(':')[0], fasta_dict[id_hla].description.split()[1].split(':')[1])
                if 'N' not in fasta_dict[id_hla].description and allele not in deja: # sélectionne les structures uniques avec 2 fields à modéliser
                    deja.append(allele)
                    ids.append(fasta_dict[id_hla].id.split(':')[1])
                    recap.write(os.popen("echo %s | sed -r 's#(HLA:)(HLA[0-9]* [A-C]\\*[0-9]*:[0-9]*)([ |:|Q|L].*)#\\2#g'" %fasta_dict[id_hla].description).read())
            elif fasta_dict[id_hla].description.split()[1][0] == 'E': # Evite de parcourir l'entiereté du fichier
                break
    print(len(ids))

    # Récupération et troncature de la séquence à utiliser pour la modélisation
    seqBaseHLA = "GSHSMRYFFTSVSRPGRGEPRFIAVGYVDDTQFVRFDSDAASQKMEPRAPWIEQEGPEYWDQETRNMKAHSQTDRANLGTLRGYYNQSEDGSHTIQIMYGCDVGPDGRFLRGYRQDAYDGKDYIALNEDLRSWTAADMAAQITKRKWEAVHAAEQRRVYLEGRCVDGLRRYLENGKETLQRTDPPKTHMTHHPISDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPKPLTLRWE"
        # séquence de base de protéine HLA pour tronquer en 5' et 3' les séquences à modéliser
    global dicSeqHLA; dicSeqHLA = {}
    for id in ids:
        # Téléchargement du fichier fasta de la protéine hla à modéliser
        if not os.path.exists("../../raw_data/fastas/%s.fasta" %id):
            os.system("wget -nv -O ../../raw_data/fastas/%s.fasta https://www.ebi.ac.uk/Tools/dbfetch/dbfetch/imgthlapro/%s/fasta?style=raw" %(id, id))

        # Récupération de la séquence brute dans le fichier (avec des parties 5' et 3' en trop)
        dicSeqHLA[id] = ""
        with open("../../raw_data/fastas/%s.fasta" %id, 'r') as fasta:
            for line in fasta:
                if not '>' in line:
                    dicSeqHLA[id]+=line.strip()

        # Alignement de la séquence brute avec la séquence de base pour écarter les parties non-modélisables
        alignments = align.localxs(seqBaseHLA, dicSeqHLA[id], -1, -1) # coût du gap de -1 seulement pour ne pas tronquer plus à cause d'une déletion proche d'une extrémité

        with open("%s.aln" %id, 'w') as resAlign:
            resAlign.write(format_alignment(*alignments[0])) # alignements[0] est un objet de type alignement
                # il faut l'écrire pour pouvoir le parser et ainsi récupérer la séquence
        with open("%s.aln" %id, 'r') as resAlign:
            dicSeqHLA[id] = resAlign.readlines()[2].split()[1]
        os.remove("%s.aln" %id)


    ### Modélisation des allèles
    listeModelesJs = json.dumps(listeModeles)
    with mp.Pool(mp.cpu_count()) as pool:
        pool.map(partial(modeliser, listeModelesJs), ids)
    os.remove("maf_templates.fasta")
    os.system("mkdir structs_%s" %geneHLA)
    os.system("for fic in HLA*.B99990001.pdb; do mv $fic $(echo $fic | sed -r 's#(.*)\.(B99990001)(\.pdb)#\./structs_%s/\\1\\3#g'); done" %geneHLA)
    os.system("tar czf /LAB-DATA/BiRD/shares/CRTI/EQ3/HLA-3Diff/working_data/hla_%s.tar.gz --remove-files ./structs_%s/" %(geneHLA, geneHLA))
