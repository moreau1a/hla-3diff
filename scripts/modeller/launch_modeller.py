#!/usr/bin/env python3

import sys
import json
from modeller import *              # Load standard Modeller classes
from modeller.automodel import *    # Load the AutoModel class

### Comparative modeling by the AutoModel class

log.minimal() # request non-verbose output

env = Environ() # create a new MODELLER environment to build this model in
env.io.atom_files_directory = ['../../raw_data/pdb_files'] # directories for input atom files

# if type(json.loads(sys.argv[2])) == str:
#     outputName = sys.argv[2]
# elif type(json.loads(sys.argv[2])) == list:
#     outputName = "multi"

a = AutoModel(env,
    alnfile=sys.argv[1], # alignment filename
    knowns=json.loads(sys.argv[2]), # code of the template
    sequence=sys.argv[3], # code of the target
    #root_name=outputName, # change the name of the output structure
    assess_methods=(assess.DOPE, assess.GA341))
#a.starting_model = 1; a.ending_model = 5 # index of the first/last model
    # (determines how many models to calculate)
#a.auto_align() # get an automatic alignment
    # prevents parallelization because multiple processes simultaneously try to use the same alignment file or family matrix
#a.set_output_model_format("MMCIF") # request mmCIF rather than PDB outputs
a.make() # do the actual comparative modeling
