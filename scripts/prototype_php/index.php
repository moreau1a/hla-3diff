<html>
  <head>
    <title>PHP Test</title>
  </head>

  <body>
    <h1>Estimation de la compatibilité entre donneur et receveur</h1>

    <form action="compat.php" method="get">
      <div>
        <label>Gene</label>
        <input type="radio" name="gene" id="geneA" value='A' checked>
        <label for="geneA">hla A</label>
        <input type="radio" name="gene" id="geneB" value='B' disabled>
        <label for="geneB">hla B</label>
        <input type="radio" name="gene" id="geneC" value='C' disabled>
        <label for="geneC">hla C</label>
      </div><br>
      <label>Alleles du receveur :</label>
      <input type="text" name="RA1a" id="RA1a" size=1 value=01> :
      <input type="text" name="RA1b" id="RA1b" size=1 value=01> &
      <input type="text" name="RA2a" id="RA2a" size=1 value=11> :
      <input type="text" name="RA2b" id="RA2b" size=1 value=01>
      <br><br>
      <label>Alleles du donneur :</label>
      <input type="text" name="DA1a" id="DA1a" size=1 value=02> :
      <input type="text" name="DA1b" id="DA1b" size=1 value=01> &
      <input type="text" name="DA2a" id="DA2a" size=1 value=01> :
      <input type="text" name="DA2b" id="DA2b" size=1 value=02>
      <br><br>
      <input type="submit">
    </form>

    <p></p>
  </body>
</html>