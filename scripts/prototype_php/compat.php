<html>
  <body>
    <?php
      $RA1 = "A*".$_GET["RA1a"].":".$_GET["RA1b"]; // Receveur allele 1
      $RA2 = "A*".$_GET["RA2a"].":".$_GET["RA2b"]; // Receveur allele 2
      $DA1 = "A*".$_GET["DA1a"].":".$_GET["DA1b"]; // Donneur allele 1
      $DA2 = "A*".$_GET["DA2a"].":".$_GET["DA2b"]; // Donneur allele 2
      /* Pour l'instant, on choisi un gène. À terme, on proposera tous les gènes en même temps */
      $gene = $_GET["gene"];
        $ficfasta = "fasta".$gene.".txt";
      $idR1 = ""; $idR2 = ""; $idD1 = ""; $idD2 = "";

      /* Recherche de l'identifiant hla du receveur et donneur */
      if (($fastafile = fopen($ficfasta, 'r')) != FALSE) {
        while ($line = fgets($fastafile)) {
          if (strpos($line, $RA1) !== FALSE) { // if x in line
            $idR1 = mb_substr($line, 0, 8);
          }
          if (strpos($line, $RA2) !== FALSE) {
            $idR2 = mb_substr($line, 0, 8);
          }
          if (strpos($line, $DA1) !== FALSE) {
            $idD1 = mb_substr($line, 0, 8);
          }
          if (strpos($line, $DA2) !== FALSE) {
            $idD2 = mb_substr($line, 0, 8);
          }
        }
      }
      if ($idR1 == "" || $idR2 == "" || $idD1 == "" || $idD2 == "") {
        exit("Au moins un allele n'existe pas dans notre base de données, veuillez rééssayer");
      } // Pourrait etre remplacé par un menu déroulant pour le choix d'allèle
      echo "Les identifiants des allèles du receveur sont ".$idR1." et ".$idR2."<br>";
      echo "Ceux du donneur sont ".$idD1." et ".$idD2."<br><br>";

      $score = 0; // Sera la somme des rmsd
      /* Identification des nouveaux allèles apportés par le doneur */
      foreach (array($idD1, $idD2) as $idD) {
        if ($idD != $idR1 && $idD != $idR2) { // L'allele est différent de ceux du donneur
          foreach (array($idR1, $idR2) as $idR) {
            /* Recherche du rmsd dans le tableau */
            $index = 0; // Numéro de colonne du premier allèle qu'on trouvera
            if (($csvfile = fopen("rmsd_hla_".$gene."_ech.csv", 'r')) != FALSE) {
              while (($data = fgetcsv($csvfile)) != FALSE) { // pour chaque ligne $data
                $num = count($data); // Nombre d'éléments dans la ligne
                if ($index == 0) { // Première ligne, on cherche le receveur ou le donneur
                  for ($c=$num-1; $c>=0; $c--) { // On lit la ligne à l'envers
                    if ($data[$c] == $idR || $data[$c] == $idD) {
                      $index = $c; // Peu importe lequel on a trouvé, on enregistre l'indice
                      break; // Pas besoin de finir la 1ere ligne
                    }
                  }
                } else { // On cherche maintenant la ligne du donneur / receveur, selon lequel on a déjà
                  // Comme, cette fois, on lit à l'endroit, on va forcément trouver l'autre
                  for ($c=0; $c < $num; $c++) {
                    if ($data[$c] != $idD && $data[$c] != $idR) {
                      break; // Pas besoin de lire cette ligne
                    } else { // On cherche la bonne case
                      $rmsd = $data[$index];
                      break 2; // Fin de lecture du csv
                    }
                  }
                }
              }
            }
            echo "Le rmsd entre ".$idD." et ".$idR." est de ".$rmsd."<br>";
            $score += $rmsd;
          }
        }
      }
      echo "Le score d'incompatibilité pour ce gène est de : ".$score;
    ?>
    <br><br>
    <form action="index.php">
      <input type="submit" value="Retourner à l'accueil">
    </form>
  </body>
</html>