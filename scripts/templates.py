import os
import requests as req
from Bio import SeqIO
from Bio import pairwise2
from Bio.pairwise2 import format_alignment


# Ce script récupère les séquences des hla sélectionnés comme templates, et vérifie ou complète l'identification allélique

out = open("../working_data/all_fasta.fasta", 'w')

# Séquence de référence choisie pour les CMH I : HLA A*01:01 (permet l'alignement pour enlever les parties 5' et 3', trop inconstantes)
baseSeq = "GSHSMRYFFTSVSRPGRGEPRFIAVGYVDDTQFVRFDSDAASQKMEPRAPWIEQEGPEYWDQETRNMKAHSQTDRANLGTLRGYYNQSEDGSHTIQIMYGCDVGPDGRFLRGYRQDAYDGKDYIALNEDLRSWTAADMAAQITKRKWEAVHAAEQRRVYLEGRCVDGLRRYLENGKETLQRTDPPKTHMTHHPISDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPKPLTLRW"

with open("../working_data/liste_hla_I.csv", 'r') as listePDB:
    for mol in listePDB:
        id = mol.split(',')[0] # identifiant pdb
        # Téléchargement des fastas
        if not os.path.exists("../raw_data/fastas/%s.fasta" %id):
            os.system("wget -nv -O ../raw_data/fastas/%s.fasta https://www.rcsb.org/fasta/entry/%s" %(id, id))
            # Autre moyen :
            #fasta = req.get("https://www.rcsb.org/fasta/entry/"+id)
            #open("../raw_data/fastas/"+id+".fasta", 'wb').write(fasta.content)

        # Lecture de la séquence dans le fasta
        with open("../raw_data/fastas/%s.fasta" %id, 'r') as fastaFile:
            fastaDict = SeqIO.to_dict(SeqIO.parse(fastaFile, "fasta"))
            seq = ""
            for k in fastaDict:
                if 'A' in fastaDict[k].description.split('|')[1]:
                    seq = fastaDict[k].seq
                    break

        # Alignement de la séquence obtenue avec la séquence de ref pour enlever les parties 5' (avant GSHS) et 3' (après TLRW)
        try:
            alignments = pairwise2.align.localxs(baseSeq, seq, -10, -1)
            # On a un objet "alignments" de type alignement. On va l'enregistrer et le charger pour accéder à la séquence
            with open("align.txt", 'w') as res:
                res.write(format_alignment(*alignments[0]))
            with open("align.txt", 'r') as res:
                seq = res.readlines()[2].strip()
                if len(seq.split()) > 1:
                    seq = seq.split()[1].strip()
        except:
            print(id) # affichage des id si l'alignement n'est pas possible

        # Identification de l'allèle de la séquence récupérée
        with open("../working_data/hla_prot_inline.fasta", 'r') as fullFasta:
            fullFastaDict = SeqIO.to_dict(SeqIO.parse(fullFasta, "fasta"))
            allele = ""
            for k in fullFastaDict.keys():
                if seq in fullFastaDict[k].seq:
                    allele = fullFastaDict[k].description.split()[1]
                    allele = allele.split(':')[0]+':'+allele.split(':')[1]
                    break

        out.write(">%s|%s\n%s\n" %(id, allele, seq))

out.close()

# Après ce script, certains des templates n'ont pas été identifiés car certaines séquences sont modifiées.
# Il faut le faire à la main (all_fasta.fasta -> all_fasta_corrected)
# Puis utiliser le script sort_all_fasta.py (all_fasta_corrected -> all_fasta_sorted)
# Enfin, séparer les molécules des fichiers qui contiennent 2 hla (all_fasta_sorted -> all_fasta_final)