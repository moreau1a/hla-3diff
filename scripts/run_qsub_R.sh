# Script options
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -M augustin.moreau1@etu.univ-nantes.fr
#$ -m ea

if [ $# != 1 ]; then
    echo "Veuillez entrer un gene comme argument pour ce programme" >&2
    exit 1
fi

./heatmap_rmsd.R $1
