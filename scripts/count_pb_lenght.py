#!/usr/bin/env python3


# Ce script compte le nombre d'acides aminés dans les 1000 premières entrées IMGT/HLA
# N'est pas nécessaire au projet

import os

d = {}
for i in range(1, 1000):
    if i > 99:
        os.system("wget -nv -O ./"+str(i)+".fasta https://www.ebi.ac.uk/Tools/dbfetch/dbfetch/imgthlapro/HLA00"+str(i)+"/fasta?style=raw")
    elif i > 9:
        os.system("wget -nv -O ./"+str(i)+".fasta https://www.ebi.ac.uk/Tools/dbfetch/dbfetch/imgthlapro/HLA000"+str(i)+"/fasta?style=raw")
    else:
        os.system("wget -nv -O ./"+str(i)+".fasta https://www.ebi.ac.uk/Tools/dbfetch/dbfetch/imgthlapro/HLA0000"+str(i)+"/fasta?style=raw")
    with open(str(i)+".fasta", 'r') as fasta:
        len_seq = fasta.readline().split()[2]
    if len_seq not in d.keys():
        d[len_seq]=1
    else:
        d[len_seq]+=1
    os.remove(str(i)+".fasta")
print(d)