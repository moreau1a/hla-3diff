dico = {}

with open("../working_data/all_fasta_corrected.fasta" ,'r') as f:
    newSeq = True
    for line in f:
        if newSeq:
            allele = line.split('|')[1]
            id = line.split('|')[0]
            newSeq = False
        else:
            dico[id] = [allele, line]
            newSeq = True

liste = (sorted(dico.items(), key = lambda t: t[1][0]))

with open("../working_data/all_fasta_sorted.fasta", 'w') as f:
    for seq in liste:
        f.write(seq[0]+"|"+seq[1][0])
        f.write(seq[1][1])