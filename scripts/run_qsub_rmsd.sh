# Script options
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -M augustin.moreau1@etu.univ-nantes.fr
#$ -m ea

conda activate hla3diff

if [ $# != 1 ]; then
    echo "Veuillez entrer un gene comme argument pour ce programme" >&2
    exit 1
fi

GENE=$1

tar xf ../working_data/hla_${GENE}.tar.gz -C ../working_data/pdb_files/
time python3 rmsd_hla_I.py ${GENE}
rm -r ../working_data/pdb_files/structs_${GENE}
