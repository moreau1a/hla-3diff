#!/mnt/c/Users/morea/AppData/Local/Schrodinger/PyMOL2/Python.exe

from pymol import cmd as pml


# Ce script compare toutes les structures d'allèles hla II entre elles et fait un tableau de rmsd.

def main():
    # Dictionnaire avec les noms de structures pour chaque allèle
    listeStruct = []; compte = {}
    with open("../working_data/liste_hla_II.csv", 'r') as listeHLA:
        for line in listeHLA:
            id = line.split(',')[0]
            if id != "6mfg": # fichier wtf avec résidues mal numérotés, inutilisable
                listeStruct.append(id)

    # Écriture d'un tableau de différences rmsd entre chaque structures
    deja = [] # liste des structures déjà traitées pour éviter de les refaire en mirroir dans le triangle bas du csv.
    with open("../results/rmsd_hla_II.csv", 'w') as r:
            for x in listeStruct:
                r.write(",%s" %x) # première ligne du tableau
            for x in listeStruct:
                obj1 = x
                pml.load("../raw_data/pdb_files/%s.cif" %obj1)
                # Sélection de la partie beta pour la superposition
                pml.create("%s_B" %obj1, "%s & c. B & i. 9-18+21-30+33-39+42-47+50-87" %obj1)

                r.write("\n"+x) # première colone du tableau
                for y in listeStruct:
                    if x == y:
                        r.write(",0") # diagonale, structure contre elle même, rmsd = 0
                    elif y in deja:
                        r.write(",") # triangle bas, vide
                    else:
                        obj2 = y
                        pml.load("../raw_data/pdb_files/%s.cif" %obj2)
                        # Sélection de la partie beta pour la superposition
                        pml.create("%s_B" %obj2, "%s & c. B & i. 9-18+21-30+33-39+42-47+50-87" %obj2)

                        try:
                            rmsdFull = pml.super("%s_B" %obj1, "%s_B" %obj2)
                        except:
                            print("Erreur de superposition entre les structures %s et %s" %(obj1, obj2))
                            exit()
                        rms = round(rmsdFull[0], 3)

                        if x not in compte.keys():
                            compte[x] = []
                        if y not in compte.keys():
                            compte[y] = []
                        compte[x].append(rms)
                        compte[y].append(rms)

                        r.write(",%s" %rms) # triangle haut
                        pml.delete(obj2)
                        pml.delete("%s_B" %obj2)
                deja.append(x)
                pml.delete(obj1)
                pml.delete("%s_B" %obj1)

main()
