from Bio import SeqIO


# Ce script créé des fichiers fasta à donner à Clustal Omega pour création des arbres phylogénétiques par alignement multiple des séquences
# des allèles dont on dispose d'une résolution structurale, ce qui permet de comparer avec ceux obtenus par comparaison des structures.

# Chargement des fastas de IMGT/HLA
with open("../working_data/hla_prot_inline.fasta", 'r') as fastaFile:
    fasta_dict = SeqIO.to_dict(SeqIO.parse(fastaFile, "fasta"))

# Écriture du premier fichier avec les hla de classe I
with open("../working_data/ClustalW_I.fasta", 'w') as clustal:
    with open("../working_data/liste_hla_I.csv", 'r') as h1:
        deja = []
        for line in h1:
            allele = "%s*%s" %(line.split(',')[1], line.split(',')[2].strip())
            if allele not in deja:
                deja.append(allele)
                for key in fasta_dict.keys():
                    if allele in fasta_dict[key].description:
                        clustal.write(">%s\n%s\n" %(allele, fasta_dict[key].seq))
                        break

# Écriture du second fichier avec les hla de classe II (chaine beta uniquement)
with open("../working_data/ClustalW_II.fasta", 'w') as clustal:
    with open("../working_data/liste_hla_II.csv", 'r') as h2:
        deja = []
        for line in h2:
            allele = "%s*%s" %(line.split(',')[3], line.split(',')[4].strip())
            if allele not in deja:
                deja.append(allele)
                for key in fasta_dict.keys():
                    if allele in fasta_dict[key].description:
                        clustal.write(">%s\n%s\n" %(allele, fasta_dict[key].seq))
                        break